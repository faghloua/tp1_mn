#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#include <x86intrin.h>

#include "poly_creux.h"


//tri d'une liste chainé 
//la fonction de tri d'une liste chainée est utilisée dans la fonction de multiplication polynomiale et dans la fonction d'initialisation des polynomes creux
void trie(p_polyf_creux_t p)
	{
	int temp;
	cellule *temp1,*temp2;
	temp1=p->tete;
	while (temp1->suivant!=NULL)
		{temp2=temp1->suivant;
		while(temp2!=NULL)
			 {
			 if (temp1->ordre<temp2->ordre)
			 	{
			 	temp=temp1->ordre;
			 	temp1->ordre=temp2->ordre;
			 	temp2->ordre=temp;
			 	}
			 temp2=temp2->suivant;
			 }
		temp1=temp1->suivant;
		}
	}


//inverse d'une liste chainée
//la fonction d'inverse est utilisée dans la fonction de la multiplication d'un polynome creux par un scalaire
p_polyf_creux_t inverse_ecr(p_polyf_creux_t p)
{
	p_polyf_creux_t p1 = p;
	p_polyf_creux_t pi = creer_polynome_creux(p->degre);
	p_polyf_creux_t p2 = creer_polynome_creux(p->degre);
	while(p1->tete != NULL)
		{
		p2->tete = p1->tete;
		p1->tete = p1->tete->suivant;
		p2->tete->suivant = pi->tete;
		pi->tete = p2->tete;
		}
	return pi;
}


p_polyf_creux_t creer_polynome_creux (int degre) 
	{
	p_polyf_creux_t p;
	p = (p_polyf_creux_t) malloc (sizeof (polyf_creux_t));
	p->degre = degre;
	p->tete = NULL;
	return p;
	}


void init_polynome_creux (p_polyf_creux_t p, float x)
{
  register unsigned int i ;
  
  if (p->tete == NULL)
  	{
  	cellule *temp = malloc(sizeof(cellule));
  	p->tete = temp;
  	temp->coeff = x;
  	temp->ordre = 0;
  	
  	for (i = 1 ; i <= p->degre ; i++)
  		{
  		cellule *cell = malloc(sizeof(cellule));
    		cell->coeff = x ;
    		cell->ordre = i;
    		cell->suivant = NULL;
    		temp->suivant = cell;
    		temp = temp->suivant;
		}
		trie(p);
  	}
  	
  else
  	{
  	cellule *temp = p->tete;
  	temp->coeff = x ;
  
  	for (i = 0 ; i < p->degre && temp->suivant != NULL; ++i)
  		{
  		temp = temp->suivant;
    		temp->coeff = x ;
		}
	}
	
  return ;
}


int egalite_polynome_creux (p_polyf_creux_t p1, p_polyf_creux_t p2)
{
  cellule *temp1 = p1->tete;
  cellule *temp2 = p2->tete;
  
  if (p1->degre != p2->degre)
  	{
  	return 0;
  	}
  
  else if (temp1->coeff != temp2->coeff)
  	{
  	return 0;
  	}
  	
  else
  {
  	while (temp1->suivant != NULL && temp2->suivant != NULL)
  	{
  		temp1 = temp1->suivant;
  		temp2 = temp2->suivant;
  		if (temp1->coeff != temp2->coeff)
  		{
  		return 0;
  		}	
  	}
  }
  
 return 1;
}


float eval_polynome_creux (p_polyf_creux_t p, float x)
{
  register unsigned int i ;
  int degre = p->degre ;
  float somme = 0 ;
  if (p==NULL)
  	{
  	exit(0);
	}
  if (p->tete==NULL)
  	{
  	return 0.0;
  	}
  cellule *temp = p->tete;
  somme = somme + ((temp->coeff) * powf(x , (float) degre)) ;
  
  for (i = degre-1 ; i >= 0 && temp->suivant != NULL; i--) 
  {
     temp = temp->suivant;
     somme = somme + ((temp->coeff) * powf(x , (float) temp->ordre)) ;
  }

  return somme ;
}


p_polyf_creux_t lire_polynome_creux_float (char *nom_fichier)
{
  FILE *f ;
  p_polyf_creux_t p ;
  int degre ;
  int i ;
  int cr ;
  float val;
  cellule *temp;
  
  f = fopen (nom_fichier, "r") ;
  
  if (f == NULL)
    {
      fprintf (stderr, "erreur ouverture %s \n", nom_fichier) ;
      exit (-1) ;
    }
  
  cr = fscanf (f, "%d", &degre) ;
  
  if (cr != 1)
    {
      fprintf (stderr, "erreur lecture du degre\n") ;
      exit (-1) ;
    }
    
  p = creer_polynome_creux (degre) ;
  
  for (i=0; i<=degre; i++)
	{
	cr = fscanf (f, "%f", &val);
	
	if (cr != 1)
		{
		fprintf (stderr, "erreur lecture coefficient %d\n", i) ;
     		exit (-1) ;
     		}
     		
     	if (val!=0.0)
     		{
     		temp = malloc(sizeof(cellule));
     		temp->coeff =  val;
     		temp->ordre = i;
     		temp->suivant = NULL;
     		  
     		if (p->tete == NULL)
     			{
     			p->tete = temp;
     			}
     		else 
     			{
     			temp->suivant = p->tete;
     			p->tete = temp;
     			}
		}
	}
  fclose (f) ;

  return p ;
}	


void ecrire_polynome_creux_float (p_polyf_creux_t p)
{
	cellule *temp = p->tete;
	
	if (temp != NULL)
		{
		printf("%f X^%d", temp->coeff, temp->ordre);
		while (temp->suivant != NULL)
			{
			temp = temp->suivant;
			printf(" + %f X^%d", temp->coeff, temp->ordre);
			}
		}
		
	else 
		{
		float x=0.0;
		printf("%f", x);
		}
		
	printf("\n");		
}


void detruire_polynome_creux (p_polyf_creux_t p) 
{
	cellule *temp;

	while(p->tete!=NULL)
		{
		temp=p->tete;
		p->tete=p->tete->suivant;
		free(temp);
		}
}


p_polyf_creux_t multiplication_polynome_creux_scalaire (p_polyf_creux_t p, float alpha)
{
	if(alpha == 0)
		{
		p_polyf_creux_t p0 = creer_polynome_creux(0);
		return p0;
		}
	
	p_polyf_creux_t p3;
	p3 = creer_polynome_creux(p->degre);
	
	cellule *cell = malloc(sizeof(cellule)); 
	cellule *temp1;
	temp1 = p->tete;
	
	while(temp1 != NULL)
		{
		cellule *temp = malloc(sizeof(cellule));
		temp->coeff = alpha * temp1->coeff;
		temp->ordre = temp1->ordre;
		temp->suivant = NULL;
	
		if (p3->tete == NULL)
     			{
     			p3->tete = temp;
     			p3->tete->suivant = NULL;
     			}
		else
			{
			cell = p3->tete;
     			p3->tete = temp;
     			p3->tete->suivant = cell;
			}
		temp1 = temp1->suivant;
		}
		
	p3 = inverse_ecr(p3);
 	return p3; 
}


p_polyf_creux_t multiplication_polynomes_creux (p_polyf_creux_t p1, p_polyf_creux_t p2) 
{
	cellule *temp2,*temp1;
	cellule *nouvelle;
	cellule *verifier;
	int trouver=0;
	p_polyf_creux_t p3;
	if(p1->tete==NULL || p2->tete==NULL)
		{
		p3=creer_polynome_creux (0);
		p3->tete=NULL;
		return p3;
		}
	p3 = creer_polynome_creux (p1->tete->ordre + p2->tete->ordre);
	temp2=p2->tete;
	temp1=p1->tete;
	while(temp1!=NULL)
		{
		temp2=p2->tete;
		while(temp2!=NULL)
			{
			nouvelle=malloc(sizeof(cellule));
			nouvelle->ordre=temp1->ordre+temp2->ordre;
			nouvelle->coeff=temp1->coeff*temp2->coeff;
			nouvelle->suivant=NULL;
			if (p3->tete == NULL)
     				{
     				p3->tete = nouvelle;
     				}
     			else 
     				{
     				verifier=p3->tete;
     				trouver=0;
     				/*nouvelle->suivant = p3->tete;
     				p3->tete = nouvelle;*/
     				while(verifier !=NULL)
     					{
     					if (verifier->ordre==nouvelle->ordre)
     						{
     						verifier->coeff=verifier->coeff+nouvelle->coeff;
     						trouver=1;
     						}
     					verifier=verifier->suivant;
     					}
     				if (trouver==0)
     					{
     					verifier=p3->tete;
     					while(verifier->suivant!=NULL)
     						{
     						verifier=verifier->suivant;
     						}
     					verifier->suivant = nouvelle;
     					}
     				}
     			temp2=temp2->suivant;
     			}
     		temp1=temp1->suivant;
		}
		trie(p3);
	return p3;
}


p_polyf_creux_t puissance_polynome_creux (p_polyf_creux_t p, int n)
	{
	cellule *nouvelle;
	p_polyf_creux_t p1;
	if (p==NULL)
		{
		exit(0);
		}
	if (n==0)
		{
		p1=creer_polynome_creux (0);
		nouvelle=malloc(sizeof(cellule));
		nouvelle->ordre=0;
		nouvelle->coeff=1;
		nouvelle->suivant=NULL;
		p1->tete=nouvelle;
		return p1;
		}
	else
		{
		if(n==1)
			{
			return p;
			}
		else
			{
			p1=p;
			for(int i=1;i<n;i++)
				{
				p1=multiplication_polynomes_creux(p,p1);
				}
			}
		}
		
	return p1;		
	}
	
	
p_polyf_creux_t addition_polynome_creux (p_polyf_creux_t p1, p_polyf_creux_t p2)
{
	cellule *temp1,*temp2,*nouvelle,*verifier;
	int trouver=0;

	p_polyf_creux_t p3;
	
	if (p1==NULL)
		{
		return p2;
		}
	if (p2==NULL)
		{
		return p1;
		}
	if (p1->tete==NULL)
		{
		return p2;
		}
	if (p2->tete==NULL)
		{
		return p1;
		}
	if (p2->tete==NULL && p1->tete==NULL)
		{
		p3=creer_polynome_creux(0);
		return p3;
		}
		
	temp1=p1->tete;
	temp2=p2->tete;
	p3=creer_polynome_creux (max(p1->tete->ordre,p2->tete->ordre));
	
	while (temp1!= NULL && temp2!=NULL)
		{
		if (temp1->ordre > temp2->ordre)
			{
			nouvelle=malloc(sizeof(cellule));
			nouvelle->ordre=temp1->ordre;
			nouvelle->coeff=temp1->coeff;
			nouvelle->suivant=NULL;
			if (p3->tete==NULL)
				{
				p3->tete = nouvelle;
				}
			else
				{
				verifier=p3->tete;
     				trouver=0;
     				/*nouvelle->suivant = p3->tete;
     				p3->tete = nouvelle;*/
     				while(verifier !=NULL)
     					{
     					if (verifier->ordre==nouvelle->ordre)
     						{
     						verifier->coeff=verifier->coeff+nouvelle->coeff;
     						trouver=1;
     						}
     					verifier=verifier->suivant;
     					}
     				if (trouver==0)
     					{
     					verifier=p3->tete;
     					while(verifier->suivant!=NULL)
     						{
     						verifier=verifier->suivant;
     						}
     					verifier->suivant = nouvelle;
     					}
				}
			temp1=temp1->suivant;
			}
		else
			{
			if(temp1->ordre < temp2->ordre)
				{
				nouvelle=malloc(sizeof(cellule));
				nouvelle->ordre=temp2->ordre;
				nouvelle->coeff=temp2->coeff;
				nouvelle->suivant=NULL;
				if (p3->tete==NULL)
					{
					p3->tete = nouvelle;
					}
				else
					{
					verifier=p3->tete;
     					trouver=0;
     					/*nouvelle->suivant = p3->tete;
     					p3->tete = nouvelle;*/
     					while(verifier !=NULL)
     						{
     						if (verifier->ordre==nouvelle->ordre)
     							{
     							verifier->coeff=verifier->coeff+nouvelle->coeff;
     							trouver=1;
     							}
     						verifier=verifier->suivant;
     						}
     					if (trouver==0)
     						{
     						verifier=p3->tete;
     						while(verifier->suivant!=NULL)
     							{
     							verifier=verifier->suivant;
     							}
     						verifier->suivant = nouvelle;
     						}
					}
				temp2=temp2->suivant;
				}
			else
				{
				nouvelle=malloc(sizeof(cellule));
				nouvelle->ordre=temp1->ordre;
				nouvelle->coeff=temp2->coeff+temp1->coeff;
				nouvelle->suivant=NULL;
				if (p3->tete==NULL)
					{
					p3->tete = nouvelle;
					}
				else
					{
					verifier=p3->tete;
     					trouver=0;
     					/*nouvelle->suivant = p3->tete;
     					p3->tete = nouvelle;*/
     					while(verifier !=NULL)
     						{
     						if (verifier->ordre==nouvelle->ordre)
     							{
     							verifier->coeff=verifier->coeff+nouvelle->coeff;
     							trouver=1;
     							}
     						verifier=verifier->suivant;
     						}
     					if (trouver==0)
     						{
     						verifier=p3->tete;
     						while(verifier->suivant!=NULL)
     							{
     							verifier=verifier->suivant;
     							}
     						verifier->suivant = nouvelle;
     						}
					}
				temp1=temp1->suivant;
				temp2=temp2->suivant;
				}
			}
		}
	while (temp1!=NULL)
		{
			nouvelle=malloc(sizeof(cellule));
			nouvelle->ordre=temp1->ordre;
			nouvelle->coeff=temp1->coeff;
			nouvelle->suivant=NULL;
			if (p3->tete==NULL)
				{
				p3->tete = nouvelle;
				}
			else
				{
				verifier=p3->tete;
     				trouver=0;
     				/*nouvelle->suivant = p3->tete;
     				p3->tete = nouvelle;*/
     				while(verifier !=NULL)
     					{
     					if (verifier->ordre==nouvelle->ordre)
     						{
     						verifier->coeff=verifier->coeff+nouvelle->coeff;
     						trouver=1;
     						}
     					verifier=verifier->suivant;
     					}
     				if (trouver==0)
     					{
     					verifier=p3->tete;
     					while(verifier->suivant!=NULL)
     						{
     						verifier=verifier->suivant;
     						}
     					verifier->suivant = nouvelle;
     					}
				}
			temp1=temp1->suivant;
		}
	while(temp2!=NULL)
		{
			nouvelle=malloc(sizeof(cellule));
			nouvelle->ordre=temp2->ordre;
			nouvelle->coeff=temp2->coeff;
			nouvelle->suivant=NULL;
			if (p3->tete==NULL)
				{
				p3->tete = nouvelle;
				}
			else
				{
				verifier=p3->tete;
     				trouver=0;
     				/*nouvelle->suivant = p3->tete;
     				p3->tete = nouvelle;*/
     				while(verifier !=NULL)
     					{
     					if (verifier->ordre==nouvelle->ordre)
     						{
     						verifier->coeff=verifier->coeff+nouvelle->coeff;
     						trouver=1;
     						}
     					verifier=verifier->suivant;
     					}
     				if (trouver==0)
     					{
     					verifier=p3->tete;
     					while(verifier->suivant!=NULL)
     						{
     						verifier=verifier->suivant;
     						}
     					verifier->suivant = nouvelle;
     					}
				}
			temp2=temp2->suivant;
		
		}
		
	return p3;
}


p_polyf_creux_t composition_polynome_creux (p_polyf_creux_t p, p_polyf_creux_t q)
{

  p_polyf_creux_t pOq = creer_polynome_creux(p->degre * q->degre);
  cellule *temp1;
  temp1 = p->tete;
  
  while(temp1 != NULL)	
  	{
  	if (temp1->coeff != 0)
  	       {
  		p_polyf_creux_t puiss = puissance_polynome_creux(q,temp1->ordre);
  		p_polyf_creux_t terme = multiplication_polynome_creux_scalaire(puiss, temp1->coeff);
  		pOq = addition_polynome_creux(pOq, terme);
  		temp1 = temp1->suivant;
  	        }
  	}
   
  return pOq ;
}




