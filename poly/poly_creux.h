/*
  polyf_t   : structure polynome
  p_polyf_t : pointeur sur un polynome
*/

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

typedef struct cellule cellule;

struct cellule {
	float coeff;
	int ordre;
	cellule *suivant;
};

typedef struct { 
  int degre ;
  cellule *tete;
} polyf_creux_t , *p_polyf_creux_t ;

p_polyf_creux_t creer_polynome_creux (int degre) ;

void init_polynome_creux (p_polyf_creux_t p, float x) ;


void detruire_polynome_creux (p_polyf_creux_t p) ;

p_polyf_creux_t lire_polynome_creux_float (char *nom_fichier) ;

void ecrire_polynome_creux_float (p_polyf_creux_t p) ;

int egalite_polynome_creux (p_polyf_creux_t p1, p_polyf_creux_t p2) ;

p_polyf_creux_t addition_polynome_creux (p_polyf_creux_t p1, p_polyf_creux_t p2) ;

p_polyf_creux_t multiplication_polynome_creux_scalaire (p_polyf_creux_t p, float alpha) ;

float eval_polynome_creux (p_polyf_creux_t p, float x) ;

p_polyf_creux_t multiplication_polynomes_creux (p_polyf_creux_t p1, p_polyf_creux_t p2) ;

p_polyf_creux_t puissance_polynome_creux (p_polyf_creux_t p, int n);
p_polyf_creux_t composition_polynome_creux (p_polyf_creux_t p, p_polyf_creux_t q) ;


