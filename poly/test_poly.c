
#include <stdio.h>
#include <stdlib.h>

#include "poly.h"
#include "poly_creux.h"


int main (int argc, char **argv)
{
  p_polyf_t p1, p2, p3;
  
  if (argc != 3)
    {
      fprintf (stderr, "deux paramètres (polynomes,fichiers) sont à passer \n") ;
      exit (-1) ;
    }
      
      
  p1 = lire_polynome_float (argv [1]) ;
  p2 = lire_polynome_float (argv [2]) ;
  printf("Voici le premeir polynome\n");
  ecrire_polynome_float (p1) ;
  printf("Voici le deuxième polynome\n");
  ecrire_polynome_float (p2) ;
 
  printf("\n\n");
  printf("l'evaluation du premier polynome par 1\n");
  printf("%f\n",eval_polynome(p1,1));
  
  
  printf("\n\n");
  printf("l'evaluation du premier polynome par 2\n");
  printf("%f\n",eval_polynome(p1,2));
  
  printf("\n\n");
  printf("si 0 alors les polynomes nous sont pas egaux si 1 alors ils le sont\n");
  printf("%d\n",egalite_polynome(p1,p2));
  
  printf("\n\n");
  printf("la multiplication scalaire du 1ér polynome par 2\n") ;
  p3 = multiplication_polynome_scalaire(p1,2);
  ecrire_polynome_float (p3) ;
  
  
  printf("\n\n");
  printf("la multiplication scalire du 2ème polynome par 3\n") ;
  p3 = multiplication_polynome_scalaire(p2,3);
  ecrire_polynome_float (p3) ;
  
  
  printf("\n\n");
  printf("la multiplication des deux polynomes\n") ;
  p3 = multiplication_polynomes(p1,p2) ;
  ecrire_polynome_float (p3) ;
  
  
  printf("\n\n");
  printf("la puissance du plyonome 1 par 0\n") ;
  p3 = puissance_polynome(p2,0) ;
  ecrire_polynome_float (p3) ;
  
  
  printf("\n\n");
  printf("la puissance du plyonome 1 par 1\n") ;
  p3 = puissance_polynome(p2,1) ;
  ecrire_polynome_float (p3) ;
  
  
  printf("\n\n");
  printf("la puissance du plyonome 1 par 2\n") ;
  p3 = puissance_polynome(p2,3) ;
  ecrire_polynome_float (p3) ;
  
  
  printf("\n\n");
  printf("la composition du polynome p1 par p2\n");
  p3 = composition_polynome(p1,p2) ;
  ecrire_polynome_float (p3) ;
  
  /*
  int i= egalite_polynome(p1, p2);
  printf("%d\n", i);
  
  p_polyf_t add = addition_polynome(p1, p2);
  ecrire_polynome_float (add);
  
  float k=2.5;
  p_polyf_t scal= multiplication_polynome_scalaire(p1, k);
  ecrire_polynome_float (scal);
  
  float j = eval_polynome (p1, 2.0);
  printf("%f\n", j);
  */
 // p_polyf_t mult = multiplication_polynomes(p1, p2);
  //ecrire_polynome_float (mult);
  /*
  p_polyf_t puiss = puissance_polynome (p1, 2);
  ecrire_polynome_float (puiss);
  
  p_polyf_t comp = composition_polynome (p1, p2);
  ecrire_polynome_float (comp);
  
  p_polyf_t k = multiplication_polynomes(p1, p2);
  ecrire_polynome_float(k);
  
  k = multiplication_polynomes(k, p2);
  ecrire_polynome_float(k);
  k = multiplication_polynomes(k, p2);
  ecrire_polynome_float(k);
 p_polyf_t pow = puissance_polynome(p1, 0);
 ecrire_polynome_float(pow);
 
 p_polyf_t compos = composition_polynome(p1, p2);
 ecrire_polynome_float(compos);
 */
  /*
    ajouter du code pour tester les fonctions
    sur les polynomes
  */
}
